package org.firstinspires.ftc.teamcode;

public enum DriveDirection {
    Forward, Backward, Left, Right
}
